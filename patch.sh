#!/bin/bash

cfg=$wd/ansible.cfg
wd=/home/foobar/ansible-patching
inv=/etc/ansible/hosts
out=/tmp/servers_`date '+%d-%m-%y'`.out
final="`grep -F -A9 '[servers]' /etc/ansible/hosts | grep -v servers`"
 
# Purged  logic for dual dates patching workaround
      cd $wd
      printf '\n'  
      echo -e "\e[34mMKII Hosts to be patched are:-\e[0m"
      printf "$final"
      printf '\n' 
        sleep 3 
        /usr/bin/ansible-playbook --extra-vars '@passwd.yml' tasks/import_servers.yml > $out
