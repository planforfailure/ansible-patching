#!/bin/bash

starttime=`date "+%d-%m-%y"`
host=`hostname -df`
DATE=`date`
output=/tmp/$host.$starttime.txt


echo "started on $DATE" > $output
printf "\n" >>  $output

#disk output
df -h >> $output
printf "\n" >>  $output


#banner
uname -a >> $output
printf "\n" >>  $output


#running processes
ps -ef >> $output
printf "\n" >>  $output


#current network ports open
ss -tupan >> $output
printf "\n" >> $output


#rpm packages
rpm -qa | sort >> $output
printf "\n" >> $output


#services running, amend for systemd
#service --status-all | grep running >> $output
#service --status-all  >> $output
systemctl list-units --type=service >> $output
printf "\n" >> $output

#pre-check update
yum check-update >> $output
printf "\n" >> $output


#requires logic/supplemental playbook
#yum upgrade >> $output

echo "completed on $DATE" >> $output

cp $output /root/yum-update-`date "+%Y%m%d"`.cap
printf "\n" >> $output

