# CentOS 7 Patching

The Server host list can be found on _jumphost_ in `/etc/ansible/hosts` under the **[servers]** inventory group. This process is deployed using Ansible Core and crond to schedule patching without any end user interaction and is completely automated from start to finish using this process.

Eventually this will move under [Ansible Controller/Automation Platform](https://www.redhat.com/en/technologies/management/ansible) when the desired framework is built and deployed. Supplemental information can be found [here]()

#### Components

The role is broken down into component tasks for each step of the patching process, with [import_servers.yml](tasks/import_servers.yml) running the individual tasks sequentionally via the `patch.sh`.

- [patch.sh](patch.sh) - calls `tasks/import_servers.yml` when ran via cron at `55 21 12,26 * *`
- [repo_copy.yml](tasks/repo_copy.yml) - Ensure required CentOS repos are used in `/etc/yum.repos.d`
- [precheck.yml](tasks/precheck.yml) - Runs precheck snapshot of the host before patching.
- [upgrade.yml](tasks/upgrade.yml) - Monthly Patching.
- [changelog.yml](tasks/changelog.yml) - Update the changelog after patching.
- [health_check.yml](tasks/health_check.yml) - Runs healthcheck on splunkd service after patching.

As these tasks require escalated privileges via sudo to run, the password is encrypted in an [ansible-vault](https://docs.ansible.com/ansible/latest/user_guide/vault.html) file `passwd.yml` which also includes some defined variables overiding earlier [precedences](https://gist.github.com/ekreutz/301c3d38a50abbaad38e638d8361a89e).

The log output can be found in `/tmp/servers_date '+%d-%m-%y'.out` on  _jumphost and in the [Splunk Index](https://)

#### Installation
If you are using the non interactive version from `patch.sh` then you would run
`$ ansible-playbook --extra-vars '@passwd.yml' tasks/import_servers.yml`

however this assumes you have the [ansible-vault](https://docs.ansible.com/ansible/latest/user_guide/vault.html) password and it has been defined in the appropiate `ansible.cfg` otherwise you need to append `--ask-vault-pass` for this too complete sucessfully.

Alternatively you can run `$ ansible-playbook tasks/import_servers.yml -bK` and provide the appopropiate escalated password for it to complete successfully.

#### Summary
A simple and automated process to easily patch Server hosts with visibility via logging and Splunk UX, also ensuring there is no outage to the service only degradation in availability of the cluster while specific nodes are patched and rebooted. 
